import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

 class Calculator extends Application {

    private TextField display;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Calculator");

        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(20, 20, 20, 20));
        gridPane.setVgap(10);
        gridPane.setHgap(10);

        display = new TextField();
        display.setEditable(false);
        display.setPrefColumnCount(4);
        gridPane.add(display, 0, 0, 4, 1);

        String[][] buttonLabels = {
                {"7", "8", "9", "/"},
                {"4", "5", "6", "*"},
                {"1", "2", "3", "-"},
                {"0", ".", "=", "+"}
        };

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                Button button = new Button(buttonLabels[i][j]);
                button.setPrefSize(50, 50);
                button.setOnAction(e -> handleButtonClick(button.getText()));
                gridPane.add(button, j, i + 1);
            }
        }

        Scene scene = new Scene(gridPane, 250, 300);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void handleButtonClick(String value) {
        switch (value) {
            case "=":
                calculateResult();
                break;
            case "+":
            case "-":
            case "*":
            case "/":
                handleOperator(value);
                break;
            case ".":
                handleDecimalPoint();
                break;
            default:
                display.appendText(value);
                break;
        }
    }

    private void handleOperator(String operator) {
        display.appendText(operator);
    }

    private void handleDecimalPoint() {
        String text = display.getText();
        if (!text.contains(".")) {
            display.appendText(".");
        }
    }

    private void calculateResult() {
        try {
            ScriptEngineManager manager = new ScriptEngineManager();
            ScriptEngine engine = manager.getEngineByName("js");

            String expression = display.getText();
            Object result = engine.eval(expression);

            display.setText(result.toString());
        } catch (ScriptException e) {
            display.setText("Error");
        }
    }
}


