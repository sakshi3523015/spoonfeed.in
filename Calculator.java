import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Calculator extends Application {

    private TextField display;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Calculator");

        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(20, 20, 20, 20));
        gridPane.setVgap(10);
        gridPane.setHgap(10);

        display = new TextField();
        display.setEditable(false);
        display.setPrefColumnCount(4);
        gridPane.add(display, 0, 0, 3, 1);
        Button eraseButton=new Button("<-");
        eraseButton.setPrefSize(50,50);
        gridPane.add(eraseButton,3,0);
        


        String[][] buttonLabels = {
                {"1", "2", "3", "/"},
                {"6", "5", "4", "*"},
                {"7", "8", "9", "-"},
                {".", "0", "=", "+"}
        };

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                Button button = new Button(buttonLabels[i][j]);
                button.setPrefSize(50, 50);
            
                button.setOnAction(e -> handleButtonClick(button.getText()));
                gridPane.add(button, j, i + 2);
            }
        }

        Scene scene = new Scene(gridPane, 250, 300);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void handleButtonClick(String value) {
        switch (value) {
            case "=":
                calculateResult();
                break;
            default:
                display.appendText(value);
                break;
        }
    }

    private void calculateResult() {
        try {
            String expression = display.getText();
            double result = evaluateExpression(expression);
            display.setText(String.valueOf(result));
        } catch (Exception e) {
            display.setText("Error");
        }
    }

    private double evaluateExpression(String expression) {
        // You can use a more sophisticated expression evaluation library here
        return Double.parseDouble(expression);
    }
}
